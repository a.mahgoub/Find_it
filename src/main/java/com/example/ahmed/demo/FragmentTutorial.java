package com.example.ahmed.demo;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by ahmed on 21/09/2017.
 */

public class FragmentTutorial extends Fragment {

    public FragmentTutorial()
    {
        //required empty constructor
    }
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return  inflater.inflate(R.layout.fragment1,container, false);
    }
}

package com.example.ahmed.demo;

import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class GameOverActivity extends AppCompatActivity {

    String Pref_name="Pref_file";
    TextView score,best_score;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_over);
        score= (TextView) findViewById(R.id.textView9);
        best_score= (TextView) findViewById(R.id.textView10);
        SharedPreferences prefs = getSharedPreferences(Pref_name, MODE_PRIVATE);
        best_score.setText(prefs.getInt("best_score", 0)+"");
        score.setText(getIntent().getExtras().get("score")+"");
    }
}
